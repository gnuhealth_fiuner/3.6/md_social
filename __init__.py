# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .social import *



def register():
  Pool.register(
    Social, 
    SocialAid,
    SThings,
    SocialThings,
    module='md_social', type_='model')