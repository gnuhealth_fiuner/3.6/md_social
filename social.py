from trytond.model import fields, ModelSQL, ModelView
from datetime import datetime, date
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool


__all__ = ['Social', 'SocialAid', 'SThings', 'SocialThings']


_states = {'readonly':Bool(Eval('social_plan'))!=True}


class Social(metaclass=PoolMeta):

    __name__ = "party.party"

    disabled = fields.Boolean('Disabled')
    disabled_des = fields.Char('Wich', states={'readonly':Bool(Eval('disabled'))!=True,
                        'required':Bool(Eval('disabled'))==True})
    
    social_plan = fields.Boolean('Social Plan')
    plan_auh = fields.Boolean('Asignación Universal por Hijo', states=_states)
    plan_pen = fields.Boolean('Pensión', states=_states)
    plan_tars = fields.Boolean('Tarjeta Social', states=_states)
    plan_tar = fields.Boolean('Tarjeta Alimentar', states=_states)
    plan_4035 = fields.Boolean('Ley 4035', states=_states)
    plan_prog = fields.Boolean('PROGRESAR', states=_states)
    plan_oth = fields.Boolean('Otro', states=_states)

    employed = fields.Boolean('Employed')
    employed_dep = fields.Selection([
        (None, ''),
    	('dependent', 'Dependent'),
    	('independent', 'In()d()ependent'),
        ], 'Kind of Employement', states={'readonly':Bool(Eval('employed'))!=True,
                        'required':Bool(Eval('employed'))==True}, sort=False)

    mensual_incom = fields.Float('Mensual incoming')

    scholar_meal = fields.Boolean('Scholar Meal')



class SocialAid(ModelSQL, ModelView):
    "SocialAid"
    
    __name__ = 'social.social_aid'

    person= fields.Many2One('party.party',"Person", domain=[('is_person', '=', True),])
    social_things = fields.Many2Many(
        'social.social_things', 'name', 's_things',
        'Elements')
    request_date = fields.Date('Date')

    @staticmethod
    def default_request_date():
        now = datetime.now()
        return now


class SThings(ModelSQL,ModelView):
    'S Things'
    __name__='social.s_things'
    
    code = fields.Char('Social Things Code', required=True)
    proc = fields.Char('Social Things')



class SocialThings(ModelSQL,ModelView):
    ' Social Things'
    __name__='social.social_things'
        
    name = fields.Many2One('social.social_aid', 'Session', ondelete='CASCADE')
    s_things = fields.Many2One('social.s_things', 'Elements', ondelete='CASCADE')



